package com.example.springbootdemo;

import com.alibaba.fastjson.JSONArray;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Stream;

/**
 * Created by chances on 2018/4/16.
 */
public class ATest {
	@Test
	public void test() {
		String[] strings = new String[]{"A", "B", "C", "D"};
		String concat = Stream.of(strings).reduce("#", (result, ele) -> result.concat("*" + ele));
		System.out.println("concat=" + concat);
		System.out.println("ab".concat("*"));
		Calendar calendar = Calendar.getInstance();
		System.out.println(calendar.getTime());
//		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.DAY_OF_YEAR, 7);
		System.out.println(calendar.getTime());
	}

	@Test
	public void strFormat() {
		String format = String.format("%05d", 1);
		System.out.println(format);
	}

	@Test
	public void parallelStreamReduce() {
		System.setProperty("java.java.util.concurrent.ForkJoinPool.common.parallelism", "12");
		String title = Arrays.stream("a,b,c,d,e,f,,,,".split(",")).parallel()
				.filter(a -> !StringUtils.isEmpty(a))
				.map(code -> {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					return code + code;
				}).reduce("", (result, ele) -> result + "#" + ele);
		System.out.println(title);
	}

	@Test
	public void ioUtils(){
		System.out.println(IOUtils.LINE_SEPARATOR_WINDOWS);
		//IOUtils.writeLines();
	}
}
