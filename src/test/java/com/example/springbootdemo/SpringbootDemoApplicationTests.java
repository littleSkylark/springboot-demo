package com.example.springbootdemo;

import com.example.springbootdemo.mapper.ProcessMapper;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;

@RunWith(SpringRunner.class)
@SpringBootTest
@MapperScan(basePackages = {"com.example.springbootdemo.mapper"})
public class SpringbootDemoApplicationTests {

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;

	@Autowired
	private ProcessMapper processMapper;

	@Test
	public void contextLoads() {
		System.out.println("proceeMapper=" + processMapper);
	}

}
