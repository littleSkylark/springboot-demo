package com.example.springbootdemo.controller;

import com.example.springbootdemo.mapper.ProcessTaskMapper;
import com.example.springbootdemo.util.Page;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by chances on 2018/4/10.
 */
@RestController
public class ListController {

	@Autowired
	private ProcessTaskMapper processTaskMapper;

	@RequestMapping({"", "/", "list"})
	public Page list(RowBounds rowBounds, HttpServletResponse response) throws IOException {
		PrintWriter writer = response.getWriter();

		writer.print("hello");
		writer.write("world");
		writer.flush();
		writer.write("!");
		writer.close();
//		processTaskMapper.selectAll();
		return null;
	}

	@RequestMapping("/download")
	public void downloadNet(HttpServletResponse response) throws Exception {
		try {
			String path = "D:/D.tar";
			File file = new File(path);
			if (file.exists()) {
				String filename = file.getName();
				InputStream fis = new BufferedInputStream(new FileInputStream(file));
				response.reset();
				response.setContentType("application/x-download");
				response.addHeader("Content-Disposition", "attachment;filename=" + new String(filename.getBytes(), "UTF-8"));
				response.addHeader("Content-Length", "" + file.length());
				OutputStream out = new BufferedOutputStream(response.getOutputStream());
				response.setContentType("application/octet-stream");
				byte[] buffer = new byte[1024 * 1024 * 10];
				int i = -1;
				int j = 0;
				while ((i = fis.read(buffer)) != -1) {
					out.write(buffer, 0, i);
					j++;
					if (j % 30 == 0) {
						System.out.println("a");
					}
				}
				fis.close();
				out.flush();
				out.close();
				try {
					response.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				PrintWriter out = response.getWriter();
				out.print("<script>");
				out.print("alert(\"not find the file\")");
				out.print("</script>");
			}
		} catch (IOException ex) {
			PrintWriter out = response.getWriter();
			out.print("<script>");
			out.print("alert(\"not find the file\")");
			out.print("</script>");
		}
	}
}
