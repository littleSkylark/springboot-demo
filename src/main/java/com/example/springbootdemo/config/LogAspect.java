package com.example.springbootdemo.config;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * Created by chances on 2018/5/15.
 */
@Aspect
@Component
@EnableAspectJAutoProxy
public class LogAspect {
}
