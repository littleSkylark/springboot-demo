package com.example.springbootdemo.mapper;

import com.example.springbootdemo.entity.ProcessTask;

import java.util.List;

public interface ProcessTaskMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ProcessTask record);

    int insertSelective(ProcessTask record);

    ProcessTask selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ProcessTask record);

    int updateByPrimaryKey(ProcessTask record);

	List<ProcessTask> selectAll();
}