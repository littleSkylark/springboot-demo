package com.example.springbootdemo.mapper;

import com.example.springbootdemo.entity.ProcessFile;

public interface ProcessFileMapper {
	int deleteByPrimaryKey(Long id);

	int insert(ProcessFile record);

	int insertSelective(ProcessFile record);

	ProcessFile selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(ProcessFile record);

	int updateByPrimaryKey(ProcessFile record);
}