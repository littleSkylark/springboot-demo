package com.example.springbootdemo.mapper;

import com.example.springbootdemo.entity.Process;

public interface ProcessMapper {
	int deleteByPrimaryKey(Long id);

	int insert(Process record);

	int insertSelective(Process record);

	Process selectByPrimaryKey(Long id);

	int updateByPrimaryKeySelective(Process record);

	int updateByPrimaryKey(Process record);

}