package com.example.springbootdemo.service;

import com.alibaba.fastjson.JSONObject;
import com.example.springbootdemo.entity.OpLog;
import org.springframework.stereotype.Service;

/**
 * Created by chances on 2018/5/15.
 */
@Service
public class OpService {
	public void save(OpLog opLog){
		System.out.println(JSONObject.toJSONString(opLog));
	}
}
