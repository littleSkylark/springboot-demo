package com.example.springbootdemo.entity;

import java.io.Serializable;
import java.util.Date;

public class Process implements Serializable {
    private Long id;

    private String processName;

    private String processdefId;

    private String processdefName;

    private Byte status;

    private Long operatorId;

    private Date createTime;

    private Date finishTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessdefId() {
        return processdefId;
    }

    public void setProcessdefId(String processdefId) {
        this.processdefId = processdefId;
    }

    public String getProcessdefName() {
        return processdefName;
    }

    public void setProcessdefName(String processdefName) {
        this.processdefName = processdefName;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Process process = (Process) o;

        if (id != null ? !id.equals(process.id) : process.id != null) return false;
        if (processName != null ? !processName.equals(process.processName) : process.processName != null) return false;
        if (processdefId != null ? !processdefId.equals(process.processdefId) : process.processdefId != null)
            return false;
        if (processdefName != null ? !processdefName.equals(process.processdefName) : process.processdefName != null)
            return false;
        if (status != null ? !status.equals(process.status) : process.status != null) return false;
        if (operatorId != null ? !operatorId.equals(process.operatorId) : process.operatorId != null) return false;
        if (createTime != null ? !createTime.equals(process.createTime) : process.createTime != null) return false;
        return finishTime != null ? finishTime.equals(process.finishTime) : process.finishTime == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (processName != null ? processName.hashCode() : 0);
        result = 31 * result + (processdefId != null ? processdefId.hashCode() : 0);
        result = 31 * result + (processdefName != null ? processdefName.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (operatorId != null ? operatorId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (finishTime != null ? finishTime.hashCode() : 0);
        return result;
    }
}