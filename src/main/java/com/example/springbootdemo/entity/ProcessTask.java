package com.example.springbootdemo.entity;

import java.io.Serializable;
import java.util.Date;

public class ProcessTask implements Serializable {
    private Long id;

    private Long processId;

    private String processName;

    private String processdefId;

    private String stepdefId;

    private String actiondefId;

    private Long roleId;

    private Byte status;

    private Byte type;

    private Boolean must;

    private String path;

    private Long operatorId;

    private Date createTime;

    private Date finishTime;

    private Long fontId;

    private String fontStepdefId;

    private String roleName;

    private String fileName;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProcessId() {
        return processId;
    }

    public void setProcessId(Long processId) {
        this.processId = processId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessdefId() {
        return processdefId;
    }

    public void setProcessdefId(String processdefId) {
        this.processdefId = processdefId;
    }

    public String getStepdefId() {
        return stepdefId;
    }

    public void setStepdefId(String stepdefId) {
        this.stepdefId = stepdefId;
    }

    public String getActiondefId() {
        return actiondefId;
    }

    public void setActiondefId(String actiondefId) {
        this.actiondefId = actiondefId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public Boolean getMust() {
        return must;
    }

    public void setMust(Boolean must) {
        this.must = must;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public Long getFontId() {
        return fontId;
    }

    public void setFontId(Long fontId) {
        this.fontId = fontId;
    }

    public String getFontStepdefId() {
        return fontStepdefId;
    }

    public void setFontStepdefId(String fontStepdefId) {
        this.fontStepdefId = fontStepdefId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}